<%-- **********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: menu tag
 ********************************************************************************************************************** --%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="petclinic" tagdir="/WEB-INF/tags" %>
<%@ attribute name="name" required="true" rtexprvalue="true"
              description="Name of the active menu: home, about-us,services, owners, vets, contact-us, careers, help, error or login" %>

<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="<spring:url value="/" htmlEscape="true" />"><span></span></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="main-navbar">
            <ul class="nav navbar-nav navbar-right">

                <petclinic:menuItem active="${name eq 'home'}" url="/" title="home page">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                    <span>Home</span>
                </petclinic:menuItem>

                 <petclinic:menuItem active="${name eq 'about-us'}" url="/about-us.html" title="About this practice">
                    <span class="glyphicon glyphicon-about" aria-hidden="true"></span>
                    <span>About Us</span>
                </petclinic:menuItem>
                
                 <petclinic:menuItem active="${name eq 'services'}" url="/services.html" title="Offered Services">
                    <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
                    <span>Services</span>
                </petclinic:menuItem>

                <petclinic:menuItem active="${name eq 'owners'}" url="/owners/find.html" title="find owners">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    <span>Find owners</span>
                </petclinic:menuItem>

                <petclinic:menuItem active="${name eq 'vets'}" url="/vets.html" title="veterinarians">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    <span>Veterinarians</span>
                </petclinic:menuItem>

                 <petclinic:menuItem active="${name eq 'contact-us'}" url="/contact-us.html" title="Contact details">
                    <span class="glyphicon glyphicon-contact" aria-hidden="true"></span>
                    <span>Contact Us</span>
                </petclinic:menuItem>

                 <petclinic:menuItem active="${name eq 'careers'}" url="/careers.html" title="Come work for us!">
                    <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
                    <span>Careers</span>
                </petclinic:menuItem>            

                 <petclinic:menuItem active="${name eq 'help'}" url="/help.html" title="Help with this website">
                    <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
                    <span>Help</span>
                </petclinic:menuItem>

                <petclinic:menuItem active="${name eq 'error'}" url="/oups.html" title="trigger a RuntimeException to see how it is handled">
                    <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                    <span>Error</span>
                </petclinic:menuItem>
                
                <petclinic:menuItem active="${name eq 'login'}" url="/login.html" title="Login as Administrator or Registered User">
                    <span class="glyphicon glyphicon-login" aria-hidden="true"></span>
                    <span>Login</span>
                </petclinic:menuItem>
            </ul>
        </div>
    </div>
</nav>
