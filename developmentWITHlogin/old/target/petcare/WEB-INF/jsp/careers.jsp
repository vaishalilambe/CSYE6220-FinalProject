<%-- **********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: careers jsp
 ********************************************************************************************************************** --%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="petclinic" tagdir="/WEB-INF/tags" %>

<petclinic:layout pageName="careers">
	<h2>Come and work with Us</h2>
		<div style="background-color:#f1f1f1 ;color:black;padding:20px;">
		  <h2>Associate Veterinarian</h2>
		  <p>Boston Petcare</p>
		  <p>Email your resume: <a href="mailto:boston@petcare.com">boston@petcare.com</a></p>
		</div>
		<div style="background-color:white ;color:black;padding:20px;">
		  <h2>Veterinarian Doctor</h2>
		  <p>California Petcare</p>
		  <p>Email your resume: <a href="mailto:california@petcare.com">california@petcare.com</a></p>
		</div>
		<div style="background-color:#f1f1f1 ;color:black;padding:20px;">
		  <h2>Client Service Representative</h2>
		  <p>Texas Petcare</p>
		  <p>Email your resume: <a href="mailto:texas@petcare.com">texas@petcare.com</a></p>
		</div>
		<div style="background-color:#white ;color:black;padding:20px;">
		  <h2>Veterinary Assistant</h2>
		  <p>California Petcare</p>
		  <p>Email your resume: <a href="mailto:california@petcare.com">california@petcare.com</a></p>
		</div>
</petclinic:layout>

