<%-- **********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: exception/error jsp
 ********************************************************************************************************************** --%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="petclinic" tagdir="/WEB-INF/tags" %>

<petclinic:layout pageName="error">

    <spring:url value="/resources/images/logo.png" var="petsImage"/>
    <img src="${petsImage}"/>

    <h2>Something went wrong, try after sometime...</h2>

    <p>${exception.message}</p>

</petclinic:layout>
