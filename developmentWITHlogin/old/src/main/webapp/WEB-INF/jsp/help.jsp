<%-- **********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: help jsp
 ********************************************************************************************************************** --%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="petclinic" tagdir="/WEB-INF/tags" %>

<petclinic:layout pageName="help">
<h2>Frequently Asked Questions: </h2>
<button class="accordion">Where can I find my pet details?</button>
<div class="panel">
  <p>Send an email with more your details, and we will assist you further.<br/>Or contact our customer care service number.</p>
</div>

<button class="accordion">Not able to send an email to Boston Office.</button>
<div class="panel">
  <p>Send an email with more your details, and we will assist you further.<br/>Or contact our customer care service number.</p>
</div>

<button class="accordion">Not able to find my pet history</button>
<div class="panel">
  <p>Send an email with more your details, and we will assist you further.</p>
</div>
<br/>
    <h3>For more information, contact our 24 X 7 customer care service: +1 800 111 1111</h3>
</petclinic:layout>

<html>
<head>
<script type="text/javascript" src="help.js"></script>
 <link href="<c:url value="help.css" />" rel="stylesheet">

</head>
</html>