<%-- **********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: about-us jsp
 ********************************************************************************************************************** --%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="petclinic" tagdir="/WEB-INF/tags" %>

<petclinic:layout pageName="about-us"> 
	<html>
	    <body>
			<div class="container">
				<header>
				   <h1>About Us</h1>
				</header> 
			    <p align="center"><iframe  width="854" height="480" src="https://www.youtube.com/embed/l8qeAiJLOTo"></iframe></p>
				<h3> Our Mission</h3>
				<p>
				    We will enhance the health and well being of all animals that come into our care;  utilizing comprehensive and collaborative, preventative medicine, as well as strategic emergency care as needed.  
				</p>
				<br />
				<h3> Our Vision</h3>
				<p>By offering conscientious care for your pets, we strive to provide a service that meets the needs of your entire family - through our medical expertise, unusually high level of customer service, and educational support.   At The Pet Clinic, it is our hope to exceed your expectations with every visit! </p>
			</div>
		</body>
	</html>
</petclinic:layout>