<%-- **********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: pivotal tag
 ********************************************************************************************************************** --%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-12 text-center"><img src="<spring:url value="/resources/images/pivotal.png" htmlEscape="true" />"
                                             alt="Petcare"/></div>
    </div>
</div>
