/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Repository class for Pet
 **********************************************************************************************************************/
package coe.neu.edu.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;

import coe.neu.edu.model.Pet;
import coe.neu.edu.model.PetType;

public interface PetRepository {


    List<PetType> findPetTypes() throws DataAccessException;

    Pet findById(int id) throws DataAccessException;

    void save(Pet pet) throws DataAccessException;

}
