package coe.neu.edu.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
