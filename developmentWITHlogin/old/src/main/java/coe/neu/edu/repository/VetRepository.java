/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Repository class for Vet
 **********************************************************************************************************************/
package coe.neu.edu.repository;

import java.util.Collection;

import org.springframework.dao.DataAccessException;

import coe.neu.edu.model.Vet;

public interface VetRepository {

    Collection<Vet> findAll() throws DataAccessException;


}
