/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Spring Data JPA specialization of the {@link PetRepository} interface
 **********************************************************************************************************************/
package coe.neu.edu.repository.datajpa;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import coe.neu.edu.model.Pet;
import coe.neu.edu.model.PetType;
import coe.neu.edu.repository.PetRepository;

public interface SpringDataPetRepository extends PetRepository, Repository<Pet, Integer> {

    @Override
    @Query("SELECT ptype FROM PetType ptype ORDER BY ptype.name")
    List<PetType> findPetTypes() throws DataAccessException;
}
