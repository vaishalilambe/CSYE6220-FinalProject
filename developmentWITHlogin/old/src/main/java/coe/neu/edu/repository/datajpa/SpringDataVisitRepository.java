/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Spring Data JPA specialization of the {@link VisitRepository} interface
 **********************************************************************************************************************/
package coe.neu.edu.repository.datajpa;

import org.springframework.data.repository.Repository;

import coe.neu.edu.model.Visit;
import coe.neu.edu.repository.VisitRepository;

public interface SpringDataVisitRepository extends VisitRepository, Repository<Visit, Integer> {
}
