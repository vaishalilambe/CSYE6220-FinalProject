<%-- **********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: services jsp
 ********************************************************************************************************************** --%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="petclinic" tagdir="/WEB-INF/tags" %>

<petclinic:layout pageName="services">
<html>
<style>
.inline-block {
   display: inline-block;
}
</style>
</html>
    <h2>Services offered at our place</h2>
<div id="banner" >
<div class="inline-block"><img class="img-responsive" alt="" src="resources/images/img2.png" style="margin-left: 10px; margin-right: 10px; float: left; width: 500px; height: 300px;">
</div>
<div class="inline-block"><img class="img-responsive" alt="" src="resources/images/img1.png" style="margin-left: 100px; margin-right: 10px; float: right; width: 500px; height: 300px;">
</div>
</div>
<br /><br />
<hr />
<ul>
	<li>Complete Companion Animal Care</li>
	<li>Dentistry and Digital Dental X-Rays</li>
	<li>Ultrasonography</li>
	<li>Digital Radiography</li>
	<li>Dietary and Nutritional Counseling</li>
	<li>Ferret, Pocket Pet and Reptile Medicine</li>
	<li>Geriatric Care</li>
	<li>Laser Surgery</li>
	<li>Endoscopy</li>
	<li>Microchipping</li>
	<li>Prescription Pet Foods</li>
	<li>In-House Laboratory Testing</li>
	<li>Wellness and Preventive Care</li>
	<li>Complete Surgical Services</li>
	<li>Therapy Laser</li>
	<li>Acupuncture</li>	
</ul>
</petclinic:layout>

