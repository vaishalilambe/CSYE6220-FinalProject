<%-- **********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: contact-us jsp
 ********************************************************************************************************************** --%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="petclinic" tagdir="/WEB-INF/tags" %>


<petclinic:layout pageName="contact-us">
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="contact.js"></script>
 <link href="<c:url value="contact.css" />" rel="stylesheet">
</head>

<body>
<header>
   <h1>Contact Us</h1>
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Boston')">Boston</button>
  <button class="tablinks" onclick="openCity(event, 'California')">California</button>
  <button class="tablinks" onclick="openCity(event, 'Texas')">Texas</button>
</div>
<div id="Boston" class="tabcontent">
  <h3>Boston</h3>
			<ul>
				<li> Email: <a href="mailto:boston@petcare.com">PetCare-Boston</a></li>
				<li> Contact#: <strong>857-555-5555</strong></li>
				<li> Fax: <strong> 402-330-9675</strong></li>
				<li> Address: <address>375, NN Cambridge, Boston MA 02142</address></li>
			</ul>			 
</div>

<div id="California" class="tabcontent">
<h3>California</h3>
  
			<ul>
				<li> Email: <a href="mailto:california@petcare.com">PetCare-California</a></li>
				<li> Contact#: <strong>916-555-5555</strong></li>
				<li> Fax: <strong> 402-330-9675</strong></li>
				<li> Address: <address>4090, Truxel, Sacramento CA 95834</address></li>
			</ul>			

</div>

<div id="Texas" class="tabcontent">
  <h3>Texas</h3>
  
			<ul>
				<li> Email: <a href="mailto:texas@petcare.com">PetCare-Texas</a></li>
				<li> Phone#: <strong>512-555-5555</strong></li>
				<li> Fax: <strong> 402-330-9675</strong></li>
				<li> Address: <address>505, AR, Texas 73301</address></li>
			</ul>			

</div>

</header>

</body> 
</petclinic:layout>

