/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Controller for Help page
 **********************************************************************************************************************/

package coe.neu.edu.web;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelpController {

    @RequestMapping(value = { "/help.html"})
    public String aboutUsPage(final Map<String, Object> model) {
    	// Simple forward to a JSP.
        return "help";
    }
}
