/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Login
 **********************************************************************************************************************/

package coe.neu.edu.web;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
public class LoginController {

	  @RequestMapping(value = { "/login.html"}, method = RequestMethod.GET)
	    public String loginPage(final Map<String, Object> model) {
	    	// Simple forward to a JSP.
	        return "login";
	}
		
}