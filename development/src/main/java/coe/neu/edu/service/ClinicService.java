/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Mostly used as a facade so all controllers have a single point of entry
 **********************************************************************************************************************/
package coe.neu.edu.service;

import java.util.Collection;

import org.springframework.dao.DataAccessException;

import coe.neu.edu.model.Owner;
import coe.neu.edu.model.Pet;
import coe.neu.edu.model.PetType;
import coe.neu.edu.model.Vet;
import coe.neu.edu.model.Visit;

public interface ClinicService {

    Collection<PetType> findPetTypes() throws DataAccessException;

    Owner findOwnerById(int id) throws DataAccessException;

    Pet findPetById(int id) throws DataAccessException;

    void savePet(Pet pet) throws DataAccessException;

    void saveVisit(Visit visit) throws DataAccessException;

    Collection<Vet> findVets() throws DataAccessException;

    void saveOwner(Owner owner) throws DataAccessException;

    Collection<Owner> findOwnerByLastName(String lastName) throws DataAccessException;

	Collection<Visit> findVisitsByPetId(int petId);

}
