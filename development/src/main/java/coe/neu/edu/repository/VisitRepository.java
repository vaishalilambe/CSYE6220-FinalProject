/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Repository class for Visit
 **********************************************************************************************************************/
package coe.neu.edu.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;

import coe.neu.edu.model.Visit;

public interface VisitRepository {

    void save(Visit visit) throws DataAccessException;

    List<Visit> findByPetId(Integer petId);

}
