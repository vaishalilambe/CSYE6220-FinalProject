/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: {@link RowMapper} implementation mapping data from a {@link ResultSet} to the corresponding properties
 * of the {@link Visit} class.
 **********************************************************************************************************************/
package coe.neu.edu.repository.jdbc;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import coe.neu.edu.model.Visit;

class JdbcVisitRowMapper implements RowMapper<Visit> {

    @Override
    public Visit mapRow(ResultSet rs, int row) throws SQLException {
        Visit visit = new Visit();
        visit.setId(rs.getInt("visit_id"));
        Date visitDate = rs.getDate("visit_date");
        visit.setDate(new Date(visitDate.getTime()));
        visit.setDescription(rs.getString("description"));
        return visit;
    }
}
