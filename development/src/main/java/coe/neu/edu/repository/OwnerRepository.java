/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Repository class for Owner
 **********************************************************************************************************************/
package coe.neu.edu.repository;

import java.util.Collection;

import org.springframework.dao.DataAccessException;


import coe.neu.edu.model.Owner;

public interface OwnerRepository {


    Collection<Owner> findByLastName(String lastName) throws DataAccessException;


    Owner findById(int id) throws DataAccessException;


    void save(Owner owner) throws DataAccessException;


}
