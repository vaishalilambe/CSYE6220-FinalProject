/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Spring Data JPA specialization of the {@link VetRepository} interface
 **********************************************************************************************************************/
package coe.neu.edu.repository.datajpa;

import org.springframework.data.repository.Repository;

import coe.neu.edu.model.Vet;
import coe.neu.edu.repository.VetRepository;

public interface SpringDataVetRepository extends VetRepository, Repository<Vet, Integer> {
}
