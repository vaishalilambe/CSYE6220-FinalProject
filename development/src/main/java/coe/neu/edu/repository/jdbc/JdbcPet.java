/**********************************************************************************************************************
 * Author:  Vaishali Lambe
 * Course:  CSYE6220 Enterprise Software Design
 * Academic Year: Spring 2018
 * Description: Subclass of Pet that carries temporary id properties which are only relevant for a JDBC implementation of the
 * PetRepository.
 **********************************************************************************************************************/
package coe.neu.edu.repository.jdbc;

import coe.neu.edu.model.Pet;

class JdbcPet extends Pet {

    private int typeId;

    private int ownerId;

    public int getTypeId() {
        return this.typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getOwnerId() {
        return this.ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

}
