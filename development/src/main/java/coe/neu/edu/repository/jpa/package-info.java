/**
 * The classes in this package represent the JPA implementation
 * of PetCare's persistence layer.
 */
package coe.neu.edu.repository.jpa;

