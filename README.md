## Introduction

This is an acdemic individual project which is about 
online care system/clinic system for pets esp. dogs, cats, birds and hamsters

## Course
 CSYE 6220 Enterprise Software Design
 
## Academic Semester:

Spring 2018

## Project Name

Web Application : Online Pet care/clinic system for pets 
It has below UI pages:
Home
About Us
Services
Find Owner
Veterinarians List
Contact Us
Careers
Help
Error
Login

## Final Project Presentation

https://prezi.com/view/PGS8xNBaQK8LFAaQKTiO/

## Video Links

- Set up

https://www.screencast.com/t/agr8SDENh

- System Requirements

https://www.screencast.com/t/iiDZnKIaVmon

- Design Specifications

https://www.screencast.com/t/7XvBsLkP5HG

- Initial Project Set up in Spring Tool Suit/Eclipse IDE 

http://www.screencast.com/t/FPmoTn6Fmgr

- UI and Controllers

http://www.screencast.com/t/RGXHPABH21m

- Form Validations

https://www.screencast.com/t/M4cHTcESDQyJ

- Final Deliverables

http://www.screencast.com/t/qgUneiIS4G

## Folder Structure in Repository
1. design_specifications [initial draft of design document]
2. system requirements [initial draft of system requirement document]
3. development [all source code]
4. testing [screenshots of test report]
5. project prsentation [url]
6. developmentWITHlogin[all source code with login embedded] -- it has some issue in login

## MySQL DB SETUP

1. Run the "src/main/resources/db/mysql/initDB.sql” script using MySQL Workbench, then run the “src/main/resources/db/mysql/populateDB.sql” script

2. Once the database is set up, the tests should work:

	mvn clean package

3. So long as the passwords in pom.xml and src/main/resources/spring/data-access.properties for the database match whatever was set up for the database

## Steps to run an application:

1. Open Terminal and go to project folder directory path

2. Clean the project
    
    ![](./images/1.png)
    ![](./images/2.png)
    
3. Run the project
    
    ![](./images/3.png)
    ![](./images/4.png)
    
4. Open browser and type URL
   
    ![](./images/5.png)
    ![](./images/6.png)
    ![](./images/7.png)

5. If you want to run it on tomcat9 server do below settings

   - Update the pom.xml file (attached).  Look at line 32, and 338-350.


   - Install Tomcat 9 somewhere.


   - Update the tomcat-users.xml file in your Tomcat 9 installation so it includes these roles:

         <role rolename="manager-gui"/>
         <role rolename="manager-script"/>
         <user username="admin" password="adminPa5sw0rd" roles="manager-gui,manager-script" />


   - Update your Maven settings file to include a reference for the Tomcat server:


        <servers>
                <server>
                        <id>Tomcat9Server</id>
                        <username>admin</username>
                        <password>adminPa5sw0rd</password>
                </server>
        </servers>

  - Make sure that the username and password match what is in the tomcat-users.xml file, and that the id in the POM matches the id in the Maven settings file.


  - Run Tomcat from the command-line inside the Tomcat installation directory:

       bin/startup.sh

  - It should display the Tomcat configuration, e.g.:

    Using CATALINA_BASE:   /Users/vaishalilambe/Java/apache-tomcat-9.0.4
    Using CATALINA_HOME:   /Users/vaishalilambe/Java/apache-tomcat-9.0.4
    Using CATALINA_TMPDIR: /Users/vaishalilambe/Java/apache-tomcat-9.0.4/temp
    Using JRE_HOME:        /Library/Java/JavaVirtualMachines/jdk1.8.0_144.jdk/Contents/Home
    Using CLASSPATH:       /Users/vaishalilambe/Java/apache-tomcat-9.0.4/bin/bootstrap.jar:/Users/vaishalilambe/Java/apache-tomcat-9.0.4/bin/tomcat-juli.jar


  - Deploy the application to Tomcat from the command-line inside the project directory:

        mvn clean package tomcat9:deploy

  - Go to the Petclinic URL in a web browser:

        http://localhost:8080/petcare

  - After making changes to the source, you can rebuild the project and update the running application by using:

        mvn clean package tomcat9:redeploy

## Author

Vaishali Lambe [vaishalilambe@gmail.com]